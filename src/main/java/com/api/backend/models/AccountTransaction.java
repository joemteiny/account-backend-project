package com.api.backend.models;

import java.util.List;

public class AccountTransaction {


	private int accountId;
	private int balance;
	private List <Transaction> transactions;
	

	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	public List<Transaction> getTransactions() {
		return transactions;
	}
	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "AccountTransaction [accountId=" + accountId + ", balance=" + balance + ", transactions=" + transactions
				+ "]";
	}


	
}
