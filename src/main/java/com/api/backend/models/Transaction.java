package com.api.backend.models;

import java.util.Date;

public class Transaction {

	private int transactionId;
	private Date transactionDate;
	private int amount;
	private int accountId;
	
	public int getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
	public Date getDate() {
		return transactionDate;
	}
	public void setDate(Date date) {
		this.transactionDate = date;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	@Override
	public String toString() {
		return "Transaction [transactionId=" + transactionId + ", date=" + transactionDate + ", amount=" + amount + ", accountId="
				+ accountId + "]";
	}

	
	
}
