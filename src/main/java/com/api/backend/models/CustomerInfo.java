package com.api.backend.models;

import java.util.List;

public class CustomerInfo {

	private int Id;
	private String Name;
	private String Surname;
	private List<AccountTransaction> accountTrans ;
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getSurname() {
		return Surname;
	}
	public void setSurname(String surname) {
		Surname = surname;
	}
	public List<AccountTransaction> getAccountTrans() {
		return accountTrans;
	}
	public void setAccountTrans(List<AccountTransaction> accountTrans) throws Exception {
		this.accountTrans = accountTrans;
	}
	
	
	@Override
	public String toString() {
		return "CustomerInfo [Id=" + Id + ", Name=" + Name + ", Surname=" + Surname + ", accountTrans=" + accountTrans
				+ "]";
	}
	
	
	
	
}
