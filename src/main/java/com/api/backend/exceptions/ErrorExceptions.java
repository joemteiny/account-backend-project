package com.api.backend.exceptions;


import org.springframework.http.HttpStatus;

public class ErrorExceptions extends Exception {
	
	private static final long serialVersionUID = 1L;
	private String errorMessage;
	private int errorCode;
	private HttpStatus errorStatus = HttpStatus.INTERNAL_SERVER_ERROR;

	public int getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public HttpStatus getErrorStatus() {
		return errorStatus;
	}

	public void setErrorStatus(HttpStatus errorStatus) {
		this.errorStatus = errorStatus;
	}

	public ErrorExceptions(HttpStatus status, String errorMessage) {
		super(errorMessage);
		this.errorCode = status.value();
		this.errorMessage = errorMessage;
		this.errorStatus = status;
	}

	public ErrorExceptions(String errorMessage) {
		super(errorMessage);
		this.errorCode = this.errorStatus.value();
		this.errorMessage = errorMessage;
		
	}
	
	public ErrorExceptions() {
		super();
	}
}
