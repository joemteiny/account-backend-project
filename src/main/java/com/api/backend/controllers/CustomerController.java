package com.api.backend.controllers;

import java.time.ZonedDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.backend.exceptions.ErrorExceptions;
import com.api.backend.models.CustomerInfo;
import com.api.backend.models.ErrorException;
import com.api.backend.services.CustomerService;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = "customerInfo", method = RequestMethod.GET)
	public ResponseEntity<Object> getCustomerInfo(@RequestParam int customerId) throws ErrorExceptions {

		try {
			CustomerInfo custInfo = customerService.getCustomerInfo(customerId);
			return new ResponseEntity<>(custInfo, HttpStatus.OK);
		} catch (ErrorExceptions e) {
			ErrorException er = new ErrorException(e.getMessage(), e.getErrorStatus(), ZonedDateTime.now());
			return new ResponseEntity<>(er, e.getErrorStatus());
		}

	}
}
