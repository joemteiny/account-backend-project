package com.api.backend.controllers;

import java.time.ZonedDateTime;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.backend.exceptions.ErrorExceptions;
import com.api.backend.models.Account;
import com.api.backend.models.ErrorException;
import com.api.backend.services.AccountService;
import com.api.backend.services.CustomerService;
import com.api.backend.services.TransactionService;

@RestController
@RequestMapping("/account")   
public class AccountTransactionController {

	@Autowired
	private AccountService accountService;

	@Autowired
	private TransactionService transactionService;

	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = "openAccount", method = RequestMethod.POST)
	public ResponseEntity<Object> createAccount(@RequestBody Map<String, Integer> req) {

		Account acc = new Account();
		int customerID = req.get("customerID");
		int initialCredit = req.get("initialCredit");

		try {
			customerService.getCustomerById(customerID);
		
			accountService.openAccount(customerID, 0);
			
			// The method below should be removed, and the auto-generated key must be returned in the insert service above, but since
			// H2 2.1.212 database doesn't support 'select SCOPE_IDENTITY()' and 'select @@identity', I added the step below like
			// a work around(not best practice) to get the last added accountID
			  
			int accountId = accountService.getLastAccount();
			acc = accountService.getAccount(accountId);

			if (initialCredit != 0) {
				transactionService.makeTransaction(accountId, initialCredit);
				acc.setBalance(acc.getBalance() + initialCredit);
				accountService.addBalance(acc);
			}

			return new ResponseEntity<>(acc, HttpStatus.OK);
		} catch (ErrorExceptions e) {
			ErrorException er = new ErrorException(e.getMessage(), e.getErrorStatus(), ZonedDateTime.now());
			return new ResponseEntity<>(er, e.getErrorStatus());
		}

	}

}
