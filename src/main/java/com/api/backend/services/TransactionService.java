package com.api.backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.backend.dao.AccountTransactionDAO;
import com.api.backend.exceptions.ErrorExceptions;



@Service
public class TransactionService {

	@Autowired
	private AccountTransactionDAO accountTran;
	
	public void makeTransaction(int accountId, Integer initialCredit) throws ErrorExceptions {
		try {
		accountTran.makeTransaction(accountId,initialCredit);
		} catch (Exception e) {
			throw new ErrorExceptions(e.getMessage());
		}
	}

}
