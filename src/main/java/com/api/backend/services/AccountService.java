package com.api.backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.api.backend.dao.AccountDAO;
import com.api.backend.exceptions.ErrorExceptions;
import com.api.backend.models.Account;

@Service
public class AccountService {

	@Autowired
	private AccountDAO accountTran;

	public int openAccount(Integer customerId, int initialCredit) throws ErrorExceptions {
		try {
			return accountTran.openAccount(customerId, initialCredit);
		} catch (Exception e) {
			throw new ErrorExceptions(e.getMessage());
		}
	}

	public Account getAccount(int accountId) throws ErrorExceptions {
		try {
			return accountTran.getAccount(accountId);
		} catch (Exception e) {
			throw new ErrorExceptions(e.getMessage());
		}
	}

	public void addBalance(Account acc) throws ErrorExceptions {

		try {
			accountTran.addBalance(acc);

		} catch (Exception e) {
			throw new ErrorExceptions(e.getMessage());
		}
	}

	public int getLastAccount() throws ErrorExceptions {

		try {
			return accountTran.getLastAccount();

		} catch (Exception e) {
			throw new ErrorExceptions(e.getMessage());
		}
	}

}
