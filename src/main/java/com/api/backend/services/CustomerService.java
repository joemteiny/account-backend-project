package com.api.backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.api.backend.dao.AccountTransactionDAO;
import com.api.backend.dao.CustomerDAO;
import com.api.backend.common.ErrorMessages;
import com.api.backend.exceptions.ErrorExceptions;
import com.api.backend.models.Customer;
import com.api.backend.models.CustomerInfo;

@Service
public class CustomerService {

	@Autowired
	private CustomerDAO cust;

	@Autowired
	private AccountTransactionDAO accTrans;

	public Customer getCustomerById(int customerID) throws ErrorExceptions {

		Customer cus = cust.getCustomerById(customerID);
		if (cus == null)
			throw new ErrorExceptions(HttpStatus.NOT_FOUND, ErrorMessages.CustomerNotFound);

		return cus;
	}

	public CustomerInfo getCustomerInfo(int customerId) throws ErrorExceptions {

		CustomerInfo cus = cust.getCustomerInfo(customerId);

		if (cus == null)
			throw new ErrorExceptions(HttpStatus.NOT_FOUND, ErrorMessages.CustomerNotFound);

		try {
			cus.setAccountTrans(accTrans.getAccountTrans(customerId));
		} catch (Exception e) {
			throw new ErrorExceptions(e.getMessage());
		}

		return cus;
	}

}
