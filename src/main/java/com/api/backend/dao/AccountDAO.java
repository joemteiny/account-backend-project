package com.api.backend.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.api.backend.dao.interfaces.IAccount;
import com.api.backend.models.Account;

@Repository
public class AccountDAO {
	
	@Autowired
	private IAccount acc;

	public int openAccount(Integer customerId, int initialCredit) {
		return acc.openAccount(customerId, initialCredit);
	}

	public Account getAccount(int accountId) {
		return acc.getAccount(accountId);
	}

	public void addBalance(Account acc) {
		 this.acc.addBalance(acc);
	}

	public int getLastAccount() {
		return acc.getLastAccount();
	}


}
