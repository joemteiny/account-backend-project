package com.api.backend.dao.interfaces;

import org.apache.ibatis.annotations.Mapper;

import com.api.backend.models.Account;

@Mapper
public interface IAccount {

	
	public int openAccount(Integer customerId, int initialCredit);

	public Account getAccount(int accountId);

	public void addBalance(Account acc);

	public int getLastAccount();

}
