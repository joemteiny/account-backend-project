package com.api.backend.dao.interfaces;

import org.apache.ibatis.annotations.Mapper;

import com.api.backend.models.Customer;
import com.api.backend.models.CustomerInfo;


@Mapper
public interface ICustomer {

	public Customer getCustomerById(int customerID);

	public CustomerInfo getCustomerInfo(int customerId);


}
