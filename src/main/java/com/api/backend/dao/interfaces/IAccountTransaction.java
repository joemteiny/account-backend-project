package com.api.backend.dao.interfaces;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.api.backend.models.AccountTransaction;
import com.api.backend.models.Transaction;

@Mapper
public interface IAccountTransaction {

	void makeTransaction(int accountId, Integer initialCredit);

	public List<AccountTransaction> getAccountTrans(int customerId);

	public List<Transaction> getTrans(int accountId);
}
