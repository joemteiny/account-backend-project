package com.api.backend.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.api.backend.dao.interfaces.ICustomer;
import com.api.backend.models.Customer;
import com.api.backend.models.CustomerInfo;

@Repository
public class CustomerDAO {

	@Autowired
	private ICustomer cus;

	public Customer getCustomerById(int customerID) {
		return cus.getCustomerById(customerID);

	}

	public CustomerInfo getCustomerInfo(int customerId) {
		return cus.getCustomerInfo(customerId);
	}

}
