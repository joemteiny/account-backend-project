package com.api.backend.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.api.backend.dao.interfaces.IAccountTransaction;
import com.api.backend.models.AccountTransaction;

@Repository
public class AccountTransactionDAO {
	
	@Autowired
	private IAccountTransaction accountTran;

	public void makeTransaction(int accountId,Integer initialCredit) {
		accountTran.makeTransaction(accountId,initialCredit);
	}

	public List<AccountTransaction> getAccountTrans(int customerId) {

		List<AccountTransaction> accTran=accountTran.getAccountTrans(customerId);
		for (AccountTransaction acc: accTran) {
			acc.setTransactions(accountTran.getTrans(acc.getAccountId()));
		}
		return accTran;
	}

}
