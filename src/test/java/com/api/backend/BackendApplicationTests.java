package com.api.backend;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.api.backend.exceptions.ErrorExceptions;
import com.api.backend.models.Account;
import com.api.backend.models.CustomerInfo;
import com.api.backend.services.AccountService;
import com.api.backend.services.CustomerService;
import com.api.backend.services.TransactionService;

@SpringBootTest
class BackendApplicationTests {

	@Autowired
	private CustomerService custServ;

	@Autowired
	private AccountService accServ;

	@Autowired
	private TransactionService tranServ;

	@Test
	void contextLoads() {
	}

	// Test get customer by Id
	public void testGetCustomerById() throws ErrorExceptions {
		assertNotNull(custServ.getCustomerById(1));
	}

	// Test CustomerInfo before opening an account
	public void testGetCustomerInfoNoAccount() throws ErrorExceptions {
		assertNull(custServ.getCustomerInfo(1).getAccountTrans());
		assertNotNull(custServ.getCustomerInfo(1).getName());
		assertNotNull(custServ.getCustomerInfo(1).getSurname());
	}

	// Test opening account + get CustomerInfo 
	public void testOpeningAccount() throws ErrorExceptions {

		assertNotNull(custServ.getCustomerById(1));
		accServ.openAccount(1, 0);
		assertEquals(1, accServ.getLastAccount());
		Account acc = accServ.getAccount(1);
		tranServ.makeTransaction(acc.getAccountId(), 1000);
		acc.setBalance(acc.getBalance() + 1000);
		assertEquals(1000, acc.getBalance());
		accServ.addBalance(acc);
		CustomerInfo custInf = custServ.getCustomerInfo(1);
		assertEquals(1000, custInf.getAccountTrans().get(0).getBalance());
		assertEquals(1000, custInf.getAccountTrans().get(0).getTransactions().get(0).getAmount());
		assertNotNull(custInf.getName());
		assertNotNull(custInf.getSurname());

	}

}
